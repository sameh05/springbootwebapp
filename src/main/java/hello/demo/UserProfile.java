package hello.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@Controller
@RestController
@RequestMapping("/api4")
@CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")




public class UserProfile{

    @Autowired


    private UserProfileRepository userProfileRepository;
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @GetMapping ("/users3")
    public List<User2> getUsers(){

        return userProfileRepository.findAll();
    }
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @GetMapping ("/user4")
    @RequestMapping(method = RequestMethod.POST,value = "/user4")
    public User2 createUser(@RequestBody User2 user2){

        return userProfileRepository.save(user2);


    }

    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @GetMapping ("/{user2Id}")
    public Optional<User2> getUser(@PathVariable Long user2Id){
        return userProfileRepository.findById(user2Id);
    }
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @GetMapping("/pop/{user2Id}")
    public boolean deleteUser(@PathVariable Long user2Id){
        userProfileRepository.deleteById(user2Id );
        return true;
    }









}