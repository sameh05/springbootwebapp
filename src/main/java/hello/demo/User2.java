package  hello.demo;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user2")
public class User2  {
    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)

    private long user2Id;

    public void setUser2Id(long user2Id) {
        this.user2Id = user2Id;
    }

    public String getUser2firstname() {
        return user2firstname;
    }

    public void setUser2firstname(String user2firstname) {
        this.user2firstname = user2firstname;
    }

    public String getUser2lastname() {
        return user2lastname;
    }

    public void setUser2lastname(String user2lastname) {
        this.user2lastname = user2lastname;
    }

    public Date getDatepub() {
        return datepub;
    }

    public void setDatepub(Date datepub) {
        this.datepub = datepub;
    }

    public String getPublication() {
        return publication;
    }

    public void setPublication(String publication) {
        this.publication = publication;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }


    @Size(min=3,max=18)
    private String user2firstname;
    private String user2lastname;

    public String getFromuser() {
        return fromuser;
    }

    public void setFromuser(String fromuser) {
        this.fromuser = fromuser;
    }

    private Date datepub;
    private String publication;
    private String message;
    private String fromuser;
    private Long userid;
    public long getDestination() {
        return destination;
    }

    public void setDestination(long destination) {
        this.destination = destination;
    }

    private long destination;




    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user2")

    private List<Voitures> voitures=new ArrayList<>();

    public User2( String user2name) {
        this.user2firstname = user2name;

    }
    public User2() {

    }


    public  List<Voitures> getvoitures(){return  voitures;}


    // Hibernate requires a no-arg constructor






    public long getUser2Id() {
        return user2Id;
    }



    public String getUser2name() {
        return user2firstname;
    }

    public void setUser2name(String user2name) {
        this.user2firstname = user2name;
    }




// Getters and Setters (Omitted for brevity)


}