package hello.demo;




import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Admin {
    @Id
    @GeneratedValue
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private String verif;





    @OneToMany(cascade = CascadeType.ALL,mappedBy = "admin")

    private List<Absence> absences=new ArrayList<>();
    public void setId(Long id) {
        this.id = id;
    }

    public Admin() {
    }

    public Admin( String firstname, String lastname ,String email ,String password ,String verif) {
        super();

        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.verif=verif;

    }

    public String getEmail() {
        return email;
    }

    public String getVerif() {
        return verif;
    }

    public void setVerif(String verif) {
        this.verif = verif;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public  String getPassword() {
        return password;
    }

    public  void setPassword(String password) {
        this.password = password;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
