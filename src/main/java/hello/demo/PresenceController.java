package hello.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/api1")
@CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")

public class PresenceController {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired

    private PresenceRepository presenceRepository;


    @GetMapping("/emailemail")
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @RequestMapping(method = RequestMethod.POST,value = "/emailemail")
    public ArrayList getEmailemail(@RequestBody String emailemail){
        User useremail=null;
        String password2Log="";
        ArrayList al=new ArrayList();
        try{
            String req="SELECT presence.mois FROM presence  WHERE  presence.bol=0 AND presence.email=?";
            al= (ArrayList) jdbcTemplate.queryForList(req ,String.class ,emailemail);
            if((al.size())!=0) {
                return al;
            }
        }
        catch (EmptyResultDataAccessException e){
            return null;}
        return null; }




    @GetMapping("/emailemailemailj")
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @RequestMapping(method = RequestMethod.POST,value = "/emailemailemailj")
    public ArrayList getEmailemailemailemail(@RequestBody String emailemail){
        User useremailemail=null;

        ArrayList al=new ArrayList();
        try{
            String req="SELECT presence.mois FROM presence  WHERE  presence.bol=1 AND presence.email=?";
            al= (ArrayList) jdbcTemplate.queryForList(req ,String.class ,emailemail);
            if((al.size())!=0) {
                return al;
            }
        }
        catch (EmptyResultDataAccessException e){
            return null;}
        return null; }


    @GetMapping("/email2")
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @RequestMapping(method = RequestMethod.POST,value = "/email2")
    public ArrayList getEmailemail2(@RequestBody String email2){
        User useremail2=null;
        String password2Log2="";
        ArrayList a2=new ArrayList();
        try{
            String req="SELECT  presence.motif  FROM presence WHERE presence.email=?";
            a2= (ArrayList) jdbcTemplate.queryForList(req ,String.class ,email2);
            if((a2.size())!=0) {
                return a2;
            }
        }
        catch (EmptyResultDataAccessException e){
            return null;}
        return null; }




    @GetMapping("/emailemailemail")
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @RequestMapping(method = RequestMethod.POST,value = "/emailemailemail")
    public ArrayList getEmailemailemail(@RequestBody String emailemailemail){
        User useremailemail=null;
        String password2Logemail="";

        try{
            String req11="SELECT user.firstname FROM user WHERE user .email=?";
            String req22="SELECT user.lastname FROM user WHERE user .email=?";
            String req33="SELECT user.email FROM user WHERE user .email=?";
            String p1=  jdbcTemplate.queryForObject(req11 ,String.class ,emailemailemail);
            String p2=  jdbcTemplate.queryForObject(req22 ,String.class ,emailemailemail);
            String p3=  jdbcTemplate.queryForObject(req33 ,String.class ,emailemailemail);
            ArrayList al1=new ArrayList();
            al1.add(p1);
            al1.add(p2);
            al1.add(p3);
            if((al1.size())!=0) {
                return al1;
            }
        }
        catch (EmptyResultDataAccessException e){
            return null;}
        return null; }


}
