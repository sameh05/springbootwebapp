package hello.demo.Bean;


import hello.demo.User;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Requestwait {
    @Id
    @GenericGenerator(name = "requestwaitIdGen",strategy = "increment")
    @GeneratedValue(generator = "requestwaitIdGen")

    private Long id;
    private Date datedebut;
    private Date datefin;
    private Time heuredebut;
    private Time heurefin;
    private String motif ;
    private String raison;
    private String urgence;
    private Integer nbjour;
    private Date datedemande;



    @ManyToOne
    @JoinColumn(name="u1")
    private User user;





    public void setId(Long id) {
        this.id = id;
    }

    public Requestwait() {
    }

    public Requestwait( Date datedebut, Date datefin ,Time heuredebut ,Time heurefin , String motif ,String raison,String urgence ,Integer nbjour,Date datedemande) {
        super();

        this.datedebut= datedebut;
        this.datefin = datefin;
        this.heuredebut = heuredebut;
        this.heurefin = heurefin;
        this.motif=motif;
        this.raison=raison;
        this.urgence=urgence;
        this.nbjour=nbjour;
        this.datedemande=datedemande;

    }

    public Long getId() {
        return id;
    }

    public Date getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(Date datedebut) {
        this.datedebut = datedebut;
    }

    public Date getDatefin() {
        return datefin;
    }

    public void setDatefin(Date datefin) {
        this.datefin = datefin;
    }

    public Time getHeuredebut() {
        return heuredebut;
    }

    public void setHeuredebut(Time heuredebut) {
        this.heuredebut = heuredebut;
    }

    public Time getHeurefin() {
        return heurefin;
    }

    public void setHeurefin(Time heurefin) {
        this.heurefin = heurefin;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getRaison() {
        return raison;
    }

    public void setRaison(String raison) {
        this.raison = raison;
    }

    public String getUrgence() {
        return urgence;
    }

    public void setUrgence(String urgence) {
        this.urgence = urgence;
    }

    public Integer getNbjour() {
        return nbjour;
    }

    public void setNbjour(Integer nbjour) {
        this.nbjour = nbjour;
    }

    public Date getDatedemande() {
        return datedemande;
    }

    public void setDatedemande(Date datedemande) {
        this.datedemande = datedemande;
    }
    public void setUser (User user) {
        this.user = user;
    }
}
