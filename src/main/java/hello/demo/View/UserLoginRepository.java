package hello.demo.View;

import hello.demo.UserLogin;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;

@ComponentScan(basePackages = "controller")
public interface UserLoginRepository extends JpaRepository<UserLogin,Long>{



}
