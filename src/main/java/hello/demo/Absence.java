package hello.demo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Absence {
    @Id
    @GenericGenerator(name = "absenceIdGen",strategy = "increment")
    @GeneratedValue(generator = "absenceIdGen")

    private Long id;
    @JsonFormat (pattern="yyyy-MM-dd")
    private Date datef;



    public String getUrgence() {
        return urgence;
    }

    public void setUrgence(String urgence) {
        this.urgence = urgence;
    }
    @JsonFormat (pattern="yyyy-MM-dd")
    private Date dated;
    private Integer nbheure;
    private Date dater;
    private  String piece;
    private String commentaire;
    private String avis;

    private  String urgence;

    private  Date datedemande;
    private Date dateretoureffectif;

    private String type ;
    private Integer confu;
    private  Integer confa;
    private  Integer refa;




   @JsonIgnore
    @ManyToOne
    @JoinColumn(name="a")
    private Admin admin;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="u")

    private User user;





    public void setId(Long id) {
        this.id = id;
    }

    public Absence() {
    }

    public Integer getConfu() {
        return confu;
    }

    public void setConfu(Integer confu) {
        this.confu = confu;
    }

    public Integer getConfa() {
        return confa;
    }

    public void setConfa(Integer confa) {
        this.confa = confa;
    }

    public Integer getRefa() {
        return refa;
    }

    public void setRefa(Integer refa) {
        this.refa = refa;
    }



    public Date getDatedemande() {
        return datedemande;
    }

    public void setDatedemande(Date datedemande) {
        this.datedemande = datedemande;
    }

    public Date getDateretoureffectif() {
        return dateretoureffectif;
    }

    public void setDateretoureffectif(Date dateretoureffectif) {
        this.dateretoureffectif = dateretoureffectif;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Date getDater() {
        return dater;
    }

    public void setDater(Date dater) {
        this.dater = dater;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getAvis() {
        return avis;
    }

    public void setAvis(String avis) {
        this.avis = avis;
    }

    public Absence(Date datef, Date dated , String urgence,  String type  , Integer nbheure , Integer confu, Integer confa, Integer refa , Date dateretoureffectif , Date datedemande ,
                   String commentaire , Date dater , String piece  , String avis) {
        super();
this.urgence=urgence;


        this.type=type;
        this.datef=datef;
        this.dated=dated;
        this.type=type;
        this.nbheure=nbheure;
        this.confu=confu;
        this.confa=confa;
        this.refa=refa;
        this.dateretoureffectif=dateretoureffectif;
        this.datedemande=datedemande;
        this.commentaire=commentaire;
        this.dater=dater;
        this.piece=piece;
        this.avis=avis;


    }

    public Long getId() {
        return id;
    }





    public Integer getNbheure() {
        return nbheure;
    }

    public void setNbheure(Integer nbheure) {
        this.nbheure = nbheure;
    }


    public String gettype() {
        return type;
    }

    public Date getDatef() {
        return datef;
    }

    public void setDatef(Date datef) {
        this.datef = datef;
    }

    public User getUser() {
        return user;
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    public Date getDated() {
        return dated;
    }

    public void setDated(Date dated) {
        this.dated = dated;
    }

    public void settype (String type) {
        this.type = type ;
    }


    public void setUser (User user) {
        this.user = user;
    }
}
