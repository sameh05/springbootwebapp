package hello.demo;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Presence {
    @Id
    @GeneratedValue
    private Long id;
    private String firstname;
    private String email;
    private String mois;
    private String jour;
    private  Integer bol;

    public void setId(Long id) {
        this.id = id;
    }

    public Presence() {
    }

    public Integer getBol() {
        return bol;
    }

    public void setBol(Integer bol) {
        this.bol = bol;
    }

    public Presence(String firstname, String mois, String email, String jour , Integer bol) {
        super();

        this.firstname = firstname;
        this.mois = mois;
        this.email = email;
        this.jour = jour;
        this.bol = bol ;
    }

    public Long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMois() {
        return mois;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }
}