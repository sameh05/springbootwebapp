package hello.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Menu {
    @Id
    @GeneratedValue
    private  int id;

    @NotNull
    private String name;

    @ManyToMany
    private List<Voitures> voiture;

    public  Menu(){



    }

    public  void addItem(Voitures item){ voiture.add(item);

    }

    public int getId(){
        return  id;
    }
    public  String getName(){
        return  name;
    }
    public void setName(String name){
        this.name=name;
    }
    public List<Voitures> getVoiture(){
        return voiture;
    }
}

