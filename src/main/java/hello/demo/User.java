package hello.demo;
import hello.demo.Bean.Request;
import hello.demo.Bean.Requestwait;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private String job;
    private Long phone;
    private String autorisation;
    private Date datenaissance;
    public List<Absence> getAbsences() {
        return absences;
    }

    public void setAbsences(List<Absence> absences) {
        this.absences = absences;
    }

    @OneToMany(cascade ={CascadeType.PERSIST,CascadeType.REMOVE},
            mappedBy = "user")


    private List<Request> request=new ArrayList<>();
    @OneToMany(cascade ={CascadeType.PERSIST,CascadeType.REMOVE},mappedBy = "user")

    private List<Requestwait> requestwait=new ArrayList<>();
    @OneToMany(cascade ={CascadeType.PERSIST,CascadeType.REMOVE},mappedBy = "user")

    private List<Absence> absences=new ArrayList<>();

    public void setId(Long id) {
        this.id = id;
    }
    public  List<Absence> getvoitures(){return  absences;}

    public User() {
    }

    public String getAutorisation() {
        return autorisation;
    }

    public void setAutorisation(String autorisation) {
        this.autorisation = autorisation;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public User(String firstname, String lastname , String email , String password , String job , Long phone , String autorisation , Date datenaissance) {
        super();

        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password = password;
        this.job=job;
        this.phone=phone;
        this.autorisation=autorisation;
        this.datenaissance=datenaissance;

    }

    public String getEmail() {
        return email;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public  String getPassword() {
        return password;
    }

    public  void setPassword(String password) {
        this.password = password;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Long getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
