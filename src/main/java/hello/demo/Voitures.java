
package  hello.demo;
import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "voiture")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Voitures {
    @Id
    @GenericGenerator(name = "voitureIdGen",strategy = "increment")
    @GeneratedValue(generator = "voitureIdGen")

    private int voitureid;

   @NotNull
    private String voituremarque;

   @NotNull
    private String voituremodele;


    @ManyToOne
    @JoinColumn(name="u2")
    private User2 user2;



    @ManyToMany(mappedBy = "voiture")
    private List<Menu> menus;


    public Voitures( String voituremarque,String voituremodele) {

        this.voituremarque=voituremarque;
        this.voituremodele=voituremodele;

    }
    public Voitures() {

    }
    public void setUser2(User2 user2) {
        this.user2 = user2;
    }








    public void setVoituremodele(String voituremodele) {
        this.voituremodele = voituremodele;
    }


    // Hibernate requires a no-arg constructor


    public int getId() {
        return voitureid;
    }



    public String getVoituremarque() {
        return voituremarque;
    }

    public String getVoituremodele() {
        return voituremodele;
    }
    public void setVoituremarque(String voituremarque) {
        this.voituremarque = voituremarque;
    }





// Getters and Setters (Omitted for brevity)


}
