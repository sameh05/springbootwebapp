package hello.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public  interface User2Repository extends JpaRepository<User2,Long> {
}
