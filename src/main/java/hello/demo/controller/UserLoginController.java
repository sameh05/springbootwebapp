package hello.demo.controller;

import hello.demo.UserLogin;
import hello.demo.View.UserLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@Component
@RestController
//@RequestMapping("/7")
@CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "/7")




public class UserLoginController{


    @Autowired

    private UserLoginRepository userLoginRepository;


    @GetMapping ("/users7")

    public List<UserLogin> getUsers(){
        return userLoginRepository.findAll();
    }
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @GetMapping("/user7/{id}")
    public Optional<UserLogin> getUser(@PathVariable Long id){
        return userLoginRepository.findById(id);
    }
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @GetMapping("/7/{id}")
    @RequestMapping(method = RequestMethod.DELETE,value = "/7/{id}")
    public boolean deleteUser(@PathVariable Long id){
        userLoginRepository.deleteById(id);
        return true;
    }
    @GetMapping ("/user27")
    @CrossOrigin(origins = "http://localhost:4100",allowedHeaders = "*")
    @RequestMapping(method = RequestMethod.POST,value = "/user27")
    public UserLogin createUser(@RequestBody UserLogin userLogin){
        //BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        //String hashedPassword = passwordEncoder.encode(userLogin.getPassword());

        //userLogin.setPassword(hashedPassword);
        System.out.println(userLogin.getPassword());

        return userLoginRepository.save(userLogin);
    }
    @GetMapping("/users37")
    @RequestMapping(method = RequestMethod.PUT,value= "/users37")
    public UserLogin updUser(@RequestBody UserLogin userLogin){


        return userLoginRepository.save(userLogin);
    }




}