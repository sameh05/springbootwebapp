
package hello.demo;


import org.hibernate.annotations.SQLInsert;
import org.hibernate.annotations.Subselect;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserProfileRepository1 extends JpaRepository<Voitures, Integer> {

//    String getVoitureInfoBymodele(String modele);

    @Query(value = "SELECT voiture.voitureid, voiture.voituremodele,voiture.u2,voiture.voituremarque from voiture where voiture.voituremarque=?1",nativeQuery = true)
    List<Voitures> findBymarque(@PathVariable String voituremarquee);
}
