package hello.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PresenceRepository extends JpaRepository<User,Long> {
}
