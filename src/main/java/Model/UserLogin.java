package Model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userLogin")
public class UserLogin {
    @Id
    @GeneratedValue
    private Long id;
    private String email;
    private String password;

    public void setId(Long id) {
        this.id = id;
    }

    public UserLogin() {
    }

    public UserLogin( String email, String password) {
        super();

        this.email = email;
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
